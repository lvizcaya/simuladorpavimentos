<?php 
namespace simulador;
use vendor\gd_gradient_fill;

/**
* Script Name: SimuladorPavimentos.php
* Description: Clase usada para generar interpretaciones gráficas de Metacrilato en base a áridos y Metacritalo en base a chips
* para la Empresa Pavitec2000 (España)
* Author: Leonardo Vizcaya (lvizcaya) at Open+Free
* Author e-mail: lvizcaya@openmasfree.com.ve
* Version: 0.1
*
* @param integer $grano, default 1, option 1-3. Indica el grueso del grano al generar la imagen.
**/
class SimuladorPavimentos 
{
	private $width; 
	private $height;

	function __construct()
	{ 
		$this->width = 500;
		$this->height = 400;
	}

    /*
    * getChips, generador de la representación gráfica para chips.
    * @param array $colors, colores a usar en el simulador, ['color1' => porcentaje1, 'color2' => porcentaje2] -> [string => integer]
    * @return json $imagen, {'status': true/false, 'message': 'mensaje', 'image': base64}
    *
    */
	public function getChips($colors)
	{
		try {
			$width = $this->width; $height = $this->height; arsort($colors);
			$n = 10000; $color = (array_keys($colors));
			$img = imagecreatetruecolor($width,$height);
			
			list($rf,$gf,$bf) = $this->hex2rgb($color[0]);
			$fondo = imagecolorallocate($img, $rf,$gf,$bf);
			imagefilledrectangle($img, 0, 0, $width, $height, $fondo);

			$this->saturacionChipsDos($color, $n, $img, $width, $height);
			$this->saturacionChipsUno($colors, $n, $img, $width, $height);

			ob_start();
			imagepng($img);
			ImageDestroy($img);
			$return = 'data:image/png;base64,'.base64_encode(ob_get_clean());
			return json_encode(array('status' => true, 'message' => 'ok', 'image' => $return));
		} catch(Exception $e) {
			return json_encode(array('status' => false, 'message' => 'error'));
		}
	}

	private function saturacionChipsUno($colors, $n, $img, $width, $height)
	{
		foreach ($colors as $color => $porcentaje) {
			$colors[$color] = round($n*($porcentaje/100));
		}
		for ($i=0; $i<$n; $i++) {
			foreach ($colors as $color => $porcentaje) {
				if ($porcentaje>0) {
					$x = rand(1, $width);
				 	$y = rand(1, $height);
					$a = rand($x-rand(5,10), $x+rand(5,10));
					$b = rand($y-rand(5,10), $y+rand(5,10));
					$c = rand($x-rand(5,10), $x+rand(5,10));
					$d = rand($y-rand(5,10), $y+rand(5,10));
					$e = rand($x-rand(5,10), $x+rand(5,10));
					$f = rand($y-rand(5,10), $y+rand(5,10));
					$g = rand($x-rand(5,10), $x+rand(5,10));
					$h = rand($y-rand(5,10), $y+rand(5,10));
					list($r2,$g2,$b2) = $this->hex2rgb($color);
					$this->imagefilledcubic($img, array($a, $b, $c, $d, $e, $f, $g, $h), ImageColorAllocate($img,$r2,$g2,$b2));
					$colors[$color] = $porcentaje - 1;
				}
			}
		}
	}

	private function saturacionChipsDos($color, $n, $img, $width, $height)
	{
		for ($i=0; $i<$n; $i++){
			$x = rand(1, $width); $y = rand(1, $height);
			$a = $x; $b = $y;
			$c = $x+(rand(-10, -5)); $d = $y+(rand(-8, -8));
			$e = $c+(rand(-10, -5)); $f = $d+(rand(5, 10));
			$g = $e+(rand(8, 10)); $h = $y+(rand(5, 10));
			list($r2,$g2,$b2) = $this->hex2rgb($color[rand(0, count($color)-1)]);
			$this->imagefilledcubic($img, array($a, $b, $c, $d, $e, $f, $g, $h), ImageColorAllocate($img,$r2,$g2,$b2));
		}
	}

    /*
    * getAridos, generador de la representación gráfica para áridos.
    * @param array $colors, colores a usar en el simulador, ['color1' => porcentaje1, 'color2' => porcentaje2] -> [string => integer]
    * @return json $imagen, {'status': true/false, 'message': 'mensaje', 'image': base64}
    *
    */
	public function getAridos($colors)
	{
		try {
			$width = $this->width; $height = $this->height; arsort($colors);
			$r = 5; $n = 30000; $c = (array_keys($colors));
			$img = imagecreatetruecolor($width,$height);

			list($rf,$gf,$bf) = $this->hex2rgb($c[0]);
			$fondo = imagecolorallocate($img, $rf,$gf,$bf);
			imagefilledrectangle($img, 0, 0, $width, $height, $fondo);

			$this->saturacionAridosUno($colors, $n, $img, $r, $width, $height);
			$this->saturacionAridosDos($colors, $n, $img, $r, $width, $height);

			ob_start();
			imagepng($img);
			ImageDestroy($img);
			$return = 'data:image/png;base64,'.base64_encode(ob_get_clean());
			return json_encode(array('status' => true, 'message' => 'Ok', 'image' => $return));
		} catch(Exception $e) {
			return json_encode(array('status' => false, 'message' => 'error'));
		}
	}

	private function saturacionAridosUno($colors, $n, $img, $r, $width, $height)
	{
		$c = (array_keys($colors));
		for ($i=0; $i<$n*2; $i++){
			$x = rand(1, $width);
		 	$y = rand(1, $height);
			list($r1,$g1,$b1) = $this->hex2rgb($c[rand(0, count($c)-1)]);
		    ImageFilledEllipse($img, $x, $y, $r, $r, ImageColorAllocate($img,$r1,$g1,$b1));
		}
	}

	private function saturacionAridosDos($colors, $n, $img, $r, $width, $height)
	{
		foreach ($colors as $color => $porcentaje) {
			$colors[$color] = round($n*($porcentaje/100));
		}
		$menor = end($colors); $resto = false;
		for ($i=0; $i<$n; $i++) {
			$sw = false;
		 	if ($resto == false) {
				foreach ($colors as $color => $porcentaje) {
					if ($porcentaje>$menor) {
						$x = rand(1, $width); $y = rand(1, $height); $sw = true;
						list($r1,$g1,$b1) = $this->hex2rgb($color);
						ImageFilledEllipse($img, $x, $y, $r, $r, ImageColorAllocate($img,$r1,$g1,$b1));
						$colors[$color] = $porcentaje - 1;
					} else {
						break;
					}
				}
			}
			if ($sw == false) {
				$resto = true;
				foreach ($colors as $color => $porcentaje) {
					if ($porcentaje>0) {
						$x = rand(1, $width);
					 	$y = rand(1, $height);
						list($r1,$g1,$b1) = $this->hex2rgb($color);
						ImageFilledEllipse($img, $x, $y, $r, $r, ImageColorAllocate($img,$r1,$g1,$b1));
						$colors[$color] = $porcentaje - 1;
					}
				}
			}
		}
	}

	private function hex2rgb($color) 
	{
		$color = str_replace('#','',$color);
		$s = strlen($color) / 3;
		$rgb[]=hexdec(str_repeat(substr($color,0,$s),2/$s));
		$rgb[]=hexdec(str_repeat(substr($color,$s,$s),2/$s));
		$rgb[]=hexdec(str_repeat(substr($color,2*$s,$s),2/$s));
		return $rgb;
	}

	/************************** Bezier ****************************/

	private function split_cubic($p, $t)
	{
	    $a_x = $p[0] + ($t * ($p[2] - $p[0]));
	    $a_y = $p[1] + ($t * ($p[3] - $p[1]));
	    $b_x = $p[2] + ($t * ($p[4] - $p[2]));
	    $b_y = $p[3] + ($t * ($p[5] - $p[3]));
	    $c_x = $p[4] + ($t * ($p[6] - $p[4]));
	    $c_y = $p[5] + ($t * ($p[7] - $p[5]));
	    $d_x = $a_x + ($t * ($b_x - $a_x));
	    $d_y = $a_y + ($t * ($b_y - $a_y));
	    $e_x = $b_x + ($t * ($c_x - $b_x));
	    $e_y = $b_y + ($t * ($c_y - $b_y));
	    $f_x = $d_x + ($t * ($e_x - $d_x));
	    $f_y = $d_y + ($t * ($e_y - $d_y));
	    return array(
	        array($p[0], $p[1], $a_x, $a_y, $d_x, $d_y, $f_x, $f_y),
	        array($f_x, $f_y, $e_x, $e_y, $c_x, $c_y, $p[6], $p[7]));
	}

	private function cubic_ok($p)
	{
	    $flatness_sq = 0.25;
	    $a_x = $p[6] - $p[0];  $a_y = $p[7] - $p[1];
	    $b_x = $p[2] - $p[0];  $b_y = $p[3] - $p[1];
	    $c_x = $p[4] - $p[6];  $c_y = $p[5] - $p[7];
	    $a_cross_b = ($a_x * $b_y) - ($a_y * $b_x);
	    $a_cross_c = ($a_x * $c_y) - ($a_y * $c_x);
	    $d_sq = ($a_x * $a_x) + ($a_y * $a_y);
	    return max($a_cross_b * $a_cross_b, $a_cross_c * $a_cross_c) < ($flatness_sq * $d_sq);
	}

	private function subdivide_cubic($p, $level)
	{
	    $max_level = 8;
	    if (($level == $max_level) || $this->cubic_ok($p)) {
	        return array();
	    }
	    list($q, $r) = $this->split_cubic($p, 0.5);
	    $v = $this->subdivide_cubic($q, $level + 1);
	    $v[] = $r[0]; /* add a point where we split the cubic */
	    $v[] = $r[1];
	    $v = array_merge($v, $this->subdivide_cubic($r, $level + 1)); 
	    return $v;
	}

	private function get_cubic_points($p)
	{
	    $v[] = $p[0];
	    $v[] = $p[1];
	    $v = array_merge($v, $this->subdivide_cubic($p, 0));
	    $v[] = $p[6];
	    $v[] = $p[7];
	    return $v;
	}

	private function imagefilledcubic($img, $p, $color)
	{
	    $v = $this->get_cubic_points($p);
	    imagefilledpolygon($img, $v, count($v) / 2, $color);
	}
}
