<?php
require_once("./simulador/SimuladorPavimentos.php");
use simulador\SimuladorPavimentos;

$simulator = $_GET["simulator"];
$getColors = $_GET["colors"];

foreach ($getColors as $value) {
	$colors[$value['color']] = $value['porciento'];
}
$simulador = new SimuladorPavimentos();
if ($simulator==1) {
	$respuesta = $simulador->getAridos($colors);
} else {
	$respuesta = $simulador->getChips($colors);
}
echo $respuesta;
